#!/bin/bash

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
test -d "$DIR/log" || mkdir -p "$DIR/log"

LOGFILE="$DIR/log/backend.log"

source "$DIR/xsanio_server_venv/bin/activate"
"$DIR/manage.py" run_xsanio_backend >> $LOGFILE
