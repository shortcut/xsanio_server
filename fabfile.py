from fabric.api import task, env, run, local, roles, cd, execute, hide, puts,\
    sudo, put
from fabric.context_managers import settings
import posixpath
import os
import re

env.project_name = 'xsanio_server'
env.git_repo = 'git@bitbucket.org:shortcut/xsanio_server.git'
env.project_dir = '/opt/xsanio_server'.format(**env)
env.virtualenv_dir = '{project_dir}/xsanio_server_venv'.format(**env)
env.activate = 'source {virtualenv_dir}/bin/activate'.format(**env)
env.settings_template = '{project_dir}/xsanio_server/settings/local.py.example'.format(**env)
env.settings_file = '{project_dir}/xsanio_server/settings/local.py'.format(**env)
env.manage = '{project_dir}/manage.py'.format(**env)
env.launchd_items_folder = '{project_dir}/launchd_items'.format(**env)
env.launchd_folder = '/Library/LaunchDaemons'
env.launcher_backend = '{launchd_folder}/ru.shortcut.xsanio_server_backend.plist'.format(**env)
env.launcher_frontend = '{launchd_folder}/ru.shortcut.xsanio_server_frontend.plist'.format(**env)
env.launchd_load = 'launchctl load'
env.launchd_unload = 'launchctl unload'
env.requirements_file = 'requirements.pip'


#==============================================================================
# Virtualenv helper
#==============================================================================


def virtualenv(command):
    with cd(env.project_dir):
        sudo(' && '.join([env.activate, command]))


#==============================================================================
# Actual tasks
#==============================================================================


@task
def start():
    """Start the server

    """
    sudo('{launchd_load} "{launcher_frontend}"'.format(**env))
    sudo('{launchd_load} "{launcher_backend}"'.format(**env))


@task
def stop():
    """Stop the server

    """
    with settings(warn_only=True):
        sudo('{launchd_unload} "{launcher_frontend}"'.format(**env))
        sudo('{launchd_unload} "{launcher_backend}"'.format(**env))


@task
def restart():
    """Stop server & start server

    """
    stop()
    start()


@task
def bootstrap():
    """Bootstrap the environment.

    """
    #   First things first
    sudo('easy_install pip')
    sudo('pip install virtualenv')

    #   Did anyone clone the project yet? If not, we can do this.
    if not os.path.exists(env.project_dir):
        sudo('mkdir -p "{project_dir}"'.format(**env))

    put('*', '{project_dir}'.format(**env), use_sudo=True)
    sudo('chmod +x "{manage}"'.format(**env))
    sudo('chmod +x "{project_dir}/run_backend.sh"'.format(**env))
    sudo('chmod +x "{project_dir}/run_web_server.sh"'.format(**env))

    #   Don't touch the bootstrapped project
    if os.path.exists(env.virtualenv_dir):
        puts('{virtualenv_dir} already exists, exiting'.format(**env))
        return

    with cd(env.project_dir):
        #   Set up virtualenv
        sudo('virtualenv {virtualenv_dir}'.format(**env))
        virtualenv('pip install -r {requirements_file}'.format(**env))

        #   Copy default settings
        sudo('cp -v "{settings_template}" "{settings_file}"'.format(**env))

        #   Initial DB and static files setup
        virtualenv('{manage} syncdb --noinput'.format(**env))
        virtualenv('{manage} collectstatic -v0 --noinput'.format(**env))

        #   Start via launchd
        sudo('cp -v "{launchd_items_folder}/ru.shortcut.xsanio_server_backend.plist" "{launchd_folder}"'.format(**env))
        sudo('cp -v "{launchd_items_folder}/ru.shortcut.xsanio_server_frontend.plist" "{launchd_folder}"'.format(**env))

    start()


@task
def remove():
    """Remove xsanio_server on target machine

    """
    stop()
    sudo('rm "{launchd_folder}/ru.shortcut.xsanio_server_backend.plist"'.format(**env))
    sudo('rm "{launchd_folder}/ru.shortcut.xsanio_server_frontend.plist"'.format(**env))
    sudo('rm -vfr {project_dir}'.format(**env))


@task
def update():
    """Update via pull from the repo

    """
    local('git pull')
    remove()
    bootstrap()
