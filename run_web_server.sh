#!/bin/bash

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
test -d "$DIR/log" || mkdir -p "$DIR/log"

LOGFILE="$DIR/log/gunicorn.log"
LOGDIR="$DIR"
NUM_WORKERS=2

source "$DIR/xsanio_server_venv/bin/activate"
cd "$DIR"
echo "Running from $DIR"
"$DIR/xsanio_server_venv/bin/gunicorn_django" -w $NUM_WORKERS \
    --log-level=error --bind=:80 \
    --log-file=$LOGFILE --pythonpath "$DIR" \
    --settings xsanio_server.settings.local  2>>$LOGFILE
