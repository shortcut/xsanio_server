.. 

xsanio_server
======================


Quickstart
----------

This app is best suited to run on Xsan metadta controller (but that's not necessary, see below). First, clone repository to /opt/xsanio_server (on the server machine). E.g. your path to requirements.txt should be /opt/xsanio_server/requirements.txt

Then, it's time to bootstrap the project!


Long and hard way
~~~~~~~~~~~~~~~~~

On remote server::

    sudo -i
    easy_install pip && pip install virtualenv
    cd /opt/xsanio_server
    virtualenv xsanio_server_venv
    source xsanio_server_venv/bin/activate
    pip install -r requirements.pip
    cp xsanio_server/settings/local.py.example xsanio_server/settings/local.py
    ./manage.py syncdb
    ./manage.py collectstatic -v0 --noinput
    cp launchd_items/* /Library/LaunchDaemons
    launchctl load /Library/LaunchDaemons/ru.shortcut.xsanio_server_backend.plist
    launchctl load /Library/LaunchDaemons/ru.shortcut.xsanio_server_frontend.plist


Quick and easy way
~~~~~~~~~~~~~~~~~~

On your working machine (OS X command line tools and git required)::

    easy_install pip && pip install fabric
    git clone https://your_bitbucket_username@bitbucket.org/shortcut/xsanio_server.git /path/to/some/folder
    cd /path/to/some/folder
    fab bootstrap:host=user@remote-host

user@remote-host: SSH connection credentials to remote server


Custom Xsan config directory
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The backend uses information in Xsan configuration files to populate the list of clients and cvlabels. By default, it'll search in 'normal' configuration locations, e.g. /Library/Preferences/Xsan/

However, since v1.4.1, it's possible to provide the path to Xsan config directory manually. This directory should contain config.plist and VOLUME_NAME.cfg files from MDC. To do this, you should set the variable XSAN_CONFIG_PATH in xsanio_server/settings/local.py like this::

    XSAN_CONFIG_PATH = "/path/to/directory"

Check xsanio_server/settings/local.py.example for an, um-m, example settings file. Note, that if XSAN_CONFIG_PATH is set to empty string ("") or points to a non-existing directory, this parameter will be ignored (a warning will be issued in the latter case, so watch out for the log files)


Installing on Windows / Linux / other
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It should work, but you'll have to adapt some things by hand:

#. Python 2.6/2.7 is required
#. virtualenv is recommended
#. On Windows you'll have to use some other web server, since Gunicorn runs on UNIX/Linux only
#. XSAN_CONFIG_PATH should be set to some directory containing config files from MDC


Common tasks (Fabric)
---------------------

Change directory to your local repo clone, as fabric looks for fabfile.py in current directory.

Restart the server::

    fab restart:host=user@remote-server

Stop server::

    fab stop:host=user@remote-server

Start server::

    fab start:host=user@remote-server

Remove xsanio_server entirely::

    fab remove:host=user@remote-server

Update xsanio_server (issues stop, remove and bootstrap)::

    fab update:host=user@remote-server
