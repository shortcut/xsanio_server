"""Settings for Development Server"""
from xsanio_server.settings.base import *   # pylint: disable=W0614,W0401

DEBUG = True
TEMPLATE_DEBUG = DEBUG

VAR_ROOT = '/var/www/xsanio_server'
MEDIA_ROOT = os.path.join(VAR_ROOT, 'uploads')
STATIC_ROOT = os.path.join(VAR_ROOT, 'static')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'xsanio_server',
#        'USER': 'dbuser',
#        'PASSWORD': 'dbpassword',
    }
}

# WSGI_APPLICATION = 'xsanio_server.wsgi.dev.application'


#   For MySQL:

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql'
#         'NAME': 'xsanio_db_name',
#         'USER': 'xsanio_db_user',
#         'PASSWORD': 'xsanio_db_password',
#         'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
#         'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
#     }
# }

#   For PostgreSQL:

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2'
#         'NAME': 'xsanio_db_name',
#         'USER': 'xsanio_db_user',
#         'PASSWORD': 'xsanio_db_password',
#         'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
#         'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
#     }
# }
