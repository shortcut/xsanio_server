from django.conf.urls.static import static
from django.conf.urls.defaults import patterns, url, include
from django.conf import settings
from django.views.generic.simple import direct_to_template
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from xsanio_server.apps.mainapp.views import volume
from xsanio_server.apps.mainapp.views import client
from xsanio_server.apps.mainapp.views import grand_total
from xsanio_server.apps.mainapp.views import home

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
   # (r'', include('xsanio_server.apps.')),
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    (r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns('',
    url(
        r'^$',
        home.home_page_view.as_view(),
        name='home_view'
    ),
    url(
        r'^volumes/$',
        volume.display_volume_list.as_view(),
        name='volume_list_view'
    ),
    url(
        r'^volumes/(?P<pk>\d+)/$',
        volume.display_volume_detail.as_view(),
        name='volume_detail_view'
    ),
    url(
        r'^volumes_ajax/(?P<pk>\d+)/$',
        volume.display_volume_detail_ajax.as_view(),
        name='volume_detail_view_ajax'
    ),
    url(
        r'^volumes/(?P<pk>\d+)/order_by/(?P<stat_type_order>.+)/$',
        volume.display_volume_detail.as_view(),
        name='volume_detail_view_sorted',
    ),
    url(
        r'^volumes_ajax/(?P<pk>\d+)/order_by/(?P<stat_type_order>.+)/$',
        volume.display_volume_detail_ajax.as_view(),
        name='volume_detail_view_sorted_ajax'
    ),
    url(
        r'^clients/$',
        client.display_client_list.as_view(),
        name='client_list_view'
    ),
    url(
        r'clients/(?P<pk>\d+)/$',
        client.display_client_detail.as_view(),
        name='client_detail_view'
    ),
    url(
        r'clients_ajax/(?P<pk>\d+)/$',
        client.display_client_detail_ajax.as_view(),
        name='client_detail_view_ajax',
    ),
    url(
        r'clients/(?P<pk>\d+)/order_by/(?P<stat_type_order>.+)/$',
        client.display_client_detail.as_view(),
        name='client_detail_view_sorted'
    ),
    url(
        r'clients_ajax/(?P<pk>\d+)/order_by/(?P<stat_type_order>.+)/$',
        client.display_client_detail_ajax.as_view(),
        name='client_detail_view_sorted_ajax',
    ),
    url(
        r'^total_stats_ajax/$',
        grand_total.display_grand_total.as_view(),
        name='total_stats_ajax_view',
    ),
    url(
        r'^about/$',
        direct_to_template,
        {'template': 'about.html'},
        name='about_view',
    ),
)

#   Remove in production
urlpatterns += staticfiles_urlpatterns()

urlpatterns += patterns('',
     url(
        r'^static/(?P<path>.*)$',
        'django.views.static.serve',
        {
            'document_root': settings.STATIC_ROOT,
            'show_indexes': True
        }
    ),
)

if settings.DEBUG and settings.MEDIA_ROOT:
    urlpatterns += static(settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT)
