from django.views.generic import ListView
from django.views.generic import DetailView
from django.views.generic.base import TemplateView
from django.shortcuts import get_object_or_404
from xsanio_server.apps.mainapp import models
import logging


LOGGER = logging.getLogger(__name__)
LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOG_LEVEL = logging.ERROR


def has_all_stats(client_data):
    for stat_type in models.stat_type.objects.all():
        stat_type_name = stat_type.name
        if stat_type_name not in client_data:
            return False
    return True


class display_volume_list(ListView):
    template_name = 'volume_list.html'
    model = models.xsan_volume


class display_volume_detail(TemplateView):
    template_name = 'volume_detail.html'

    def get_context_data(self, **kwargs):
        context = super(display_volume_detail, self).get_context_data(**kwargs)

        vol_pk = self.kwargs.get('pk')

        context['order_by'] = self.kwargs.get('stat_type_order')
        context['pk'] = vol_pk
        context['vol_name'] = get_object_or_404(
            models.xsan_volume,
            pk=vol_pk
        ).name

        return context


class display_volume_detail_ajax(DetailView):
    template_name = 'volume_detail_ajax.html'
    model = models.xsan_volume

    def get_context_data(self, **kwargs):
        logging.basicConfig(level=LOG_LEVEL, format=LOG_FORMAT)

        # Call the base implementation first to get a context
        context = super(display_volume_detail_ajax, self).get_context_data(**kwargs)

        #   This will be passed to the template
        all_clients_data = []
        global_stats = {}
        xsan_volume = get_object_or_404(
            models.xsan_volume,
            pk=self.kwargs['pk']
        )

        #   Now we read and compile the I/O data for all present clients
        for client in models.xsanio_client.objects.active_clients().select_related():
            LOGGER.info(
                'Calculating total stats for client %s',
                client.host_name
            )
            client_data = {}
            client_data['client_hostname'] = client.host_name
            client_data['client_id'] = client.id

            #   Getting stat entries for this volume and reachable clients:
            client_stat_entries = client.xsan_volume_stat_entries(xsan_volume)

            for client_stat_entry in client_stat_entries:
                LOGGER.info(
                    'Adding stat entry: %s',
                    client_stat_entry.type.name
                )
                stat_type = client_stat_entry.type.name
                stat_value = client_stat_entry.value
                client_data[stat_type] = (
                    client_data.get(stat_type, 0) + stat_value
                )

            all_clients_data.append(client_data)

        for client_data in all_clients_data:
            for stat_type in client_data:
                stat_value = client_data[stat_type]
                #   Probably not the best way to do this...
                try:
                    global_stats[stat_type] = (
                        global_stats.get(stat_type, 0.0) + stat_value
                    )
                except TypeError:
                    #   This isn't a float, so screw it
                    pass

        #   Remove clients that don't have all stats defined
        #   TODO: find a better way to resolve this!
        all_clients_data[:] = [x for x in all_clients_data if has_all_stats(x)]

        #   Sorting!
        if 'stat_type_order' in self.kwargs:
            stat_type_order = self.kwargs['stat_type_order']
            all_clients_data = reversed(
                sorted(all_clients_data, key=lambda k: k[stat_type_order])
            )

        context['clients_data'] = all_clients_data
        context['global_stats'] = global_stats
        return context
