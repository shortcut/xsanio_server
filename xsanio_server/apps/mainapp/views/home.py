from django.views.generic.base import TemplateView


class home_page_view(TemplateView):

    template_name = 'home.html'
