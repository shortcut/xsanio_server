from django.views.generic.base import TemplateView
from xsanio_server.apps.mainapp import models


class display_grand_total(TemplateView):
    template_name = 'grand_total_ajax.html'

    def get_context_data(self, **kwargs):
        context = super(display_grand_total, self).get_context_data(**kwargs)
        global_stats = {}

        #   We need to filter all stat entries, where:
        #       stat_entry.host.is_unreachable == False
        #       stat_entry.disk is in cvlabel_entry.objects.filter(host=stat_entry.host)
        #       This way we get stat entries of all active clients that represent an Xsan LUN
        for client in models.xsanio_client.objects.active_clients().select_related():
            client_stat_entries = client.xsan_stat_entries()

            #   TODO: aggregate here
            for client_stat_entry in client_stat_entries:
                stat_type = client_stat_entry.type.name
                global_stats[stat_type] = (
                    global_stats.get(stat_type, 0) + client_stat_entry.value
                )

        context['global_stats'] = global_stats
        return context
