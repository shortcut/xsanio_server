from django.views.generic import ListView
from django.views.generic import DetailView
from django.views.generic.base import TemplateView
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
from xsanio_server.apps.mainapp import models


class display_client_list(ListView):
    template_name = 'client_list.html'
    model = models.xsanio_client


class display_client_detail_ajax(DetailView):
    template_name = 'client_detail_ajax.html'
    model = models.xsanio_client

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(
            display_client_detail_ajax,
            self,
        ).get_context_data(**kwargs)

        disks_iodata = []

        client = get_object_or_404(
            models.xsanio_client.objects.select_related(),
            pk=self.kwargs['pk']
        )

        for disk_name in client.disks():
            disk_iodata = {}
            for stat_entry in client.disk_stats(disk_name):
                disk_iodata[stat_entry.type.name] = stat_entry.value

            try:
                disk_cvlabel = models.cvlabel_entry.objects.get(
                    host=client,
                    disk=disk_name,
                )
                disk_name = disk_cvlabel.label
            except ObjectDoesNotExist:
                pass

            disk_iodata['disk_name'] = disk_name
            disks_iodata.append(disk_iodata)

        #   Sorting!
        if 'stat_type_order' in self.kwargs:
            stat_type_order = self.kwargs['stat_type_order']
            disks_iodata[:] = reversed(
                sorted(disks_iodata, key=lambda k: k[stat_type_order])
            )

        global_stats = dict(
            [
                (
                    type_name,
                    sum(
                        [d.get(type_name, 0.0) for d in disks_iodata]
                    )
                )
                for type_name in models.stat_type.objects.name_list()
            ]
        )

        context['disks_iodata'] = disks_iodata
        context['global_stats'] = global_stats
        return context


class display_client_detail(TemplateView):
    template_name = 'client_detail.html'

    def get_context_data(self, **kwargs):
        context = super(display_client_detail, self).get_context_data(**kwargs)
        context['order_by'] = self.kwargs.get('stat_type_order')
        client_pk = self.kwargs.get('pk')

        if client_pk:
            context['pk'] = client_pk
            context['client_name'] = get_object_or_404(
                models.xsanio_client,
                pk=client_pk,
            ).host_name

        return context
