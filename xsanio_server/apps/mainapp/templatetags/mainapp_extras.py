from django import template


register = template.Library()


@register.filter
def to_megabytes(bytes):
    try:
        return '%.*f' % (1, bytes / 1048576)
    except:
        return ''
