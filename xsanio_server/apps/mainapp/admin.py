from django.contrib import admin
from xsanio_server.apps.mainapp.models import *

admin.site.register(stat_type)
admin.site.register(xsanio_client)
admin.site.register(stat_entry)
admin.site.register(cvlabel_entry)
admin.site.register(xsan_volume)
admin.site.register(xsan_volume_lun)
