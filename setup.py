#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='xsanio_server',
    version='1.0',
    description="",
    author="Shortcut",
    author_email='support@shortcut.ru',
    url='',
    packages=find_packages(),
    package_data={
        'xsanio_server':
            [
                'static/*.*',
                'templates/*.*',
                'apps/mainapp/fixtures/*.*',
                'apps/mainapp/static/*.*',
                'apps/backend/test_data/test_xsan_configs/*.*',
            ],
    },
    scripts=[
        'manage.py',
        'run_backend.sh',
        'run_web_server.sh',
        'bootstrap.sh',
    ],
)
